require_relative 'elevator_builder'

elevator_builder = ElevatorBuilder.new
elevator = elevator_builder.elevator

elevator.add_passengers(1)
move_to_floor_number = ControlPanel.choose_floor_number
elevator.go_to(move_to_floor_number)
elevator.free_passengers(1)
elevator.add_passengers(5)
move_to_floor_number = ControlPanel.choose_floor_number
elevator.go_to(move_to_floor_number)
elevator.free_passengers(6)
move_to_floor_number = ControlPanel.choose_floor_number
elevator.go_to(move_to_floor_number)