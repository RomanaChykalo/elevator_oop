require_relative 'control_panel'
require_relative 'engine'
require_relative 'display'
class Elevator
    include ControlPanel
    AVR_PERSON_WEIGHT = 60
    attr_accessor :external_display,:internal_display,:passengers,:capacity,:current_floor,:total_weight
    def initialize(external_display,internal_display)
        @engines = [Engine.new(1), Engine.new(2)]
        @external_display = external_display
        @internal_display = internal_display
        @passengers = 0
        @capacity = 5
        @total_weight=0
        @current_floor = self.external_display.current_floor
    end
    public
    def go_to(move_to_floor_number)
        unless FLOOR_NUMBER.include?(move_to_floor_number)
            puts "There are not such floor number, only: "+FLOOR_NUMBER.to_s
        else
            if (move_to_floor_number!=@current_floor)
                @engines[0].turn_on();
                puts "First motor is turned on"
                if @passengers>3
                    @engines[1].turn_on();
                    puts "Powerful(second) motor is turned on, there are: "+@passengers.to_s+" person/persons in elevator"
                end
            else
                puts "You are already on #{move_to_floor_number}"
            end
        end
        internal_display.move_to_floor_number = move_to_floor_number
        internal_display.current_floor = current_floor
        self.internal_display.show_display_info
        @current_floor = move_to_floor_number
    end

    def add_passengers(num_of_people)
        current_max_pass_to_take = @capacity-@passengers
        if (current_max_pass_to_take>=num_of_people)
            door_action(true)
            @passengers = @passengers + num_of_people
            @total_weight=total_weight+num_of_people*AVR_PERSON_WEIGHT
            puts("Elevator: adds [#{num_of_people}] person")
            door_action(false)
            internal_display.total_weight = total_weight
            internal_display.passengers = passengers
        else puts "Elevator is full, try another time"
        end
    end
    def free_passengers(num_of_people)
        if (@passengers>=num_of_people)
            door_action(true)
            @passengers = @passengers - num_of_people
            @total_weight=total_weight-num_of_people*AVR_PERSON_WEIGHT
            puts("Elevator: free #{num_of_people} person")
            door_action(false)
            internal_display.total_weight = total_weight
            internal_display.passengers = passengers
        else puts "Thera are not anought people in elevator now, try another time"
        end
    end
end

