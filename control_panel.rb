module ControlPanel
    FLOOR_NUMBER=[1,2,3,4,5]
    public
    def self.choose_floor_number()
        puts "Select floor you want to visit [1] [2] [3] [4] [5] or exit to quit"
        move_to_floor_number = gets.chomp.to_i
            unless FLOOR_NUMBER.include?(move_to_floor_number)
                raise ArgumentError.new("There are not such available floor number, only: "+FLOOR_NUMBER.to_s)
            end
        return move_to_floor_number
    end

    
    def call_help
        puts "Help is calling!!!!"
    end
   
    def door_action(user_decision)
        if (user_decision)
            puts "Door is open"
        else
            puts "Door is close"
        end
    end
end
