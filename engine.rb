class Engine
    def initialize(power)
        @power = power
    end
    def turn_on()
        @isOn = true;
    end 
    def turn_off()
        @isOn = false;
    end        
end
