require_relative 'control_panel'
require_relative 'engine'
require_relative 'display'
require_relative 'elevator'
class ElevatorBuilder
    attr_accessor :elevator
    def initialize
        external_display= ExternalDisplay.new()
        internal_display= InternalDisplay.new()
        @elevator=Elevator.new(external_display,internal_display)
    end
end