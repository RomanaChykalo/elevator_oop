require_relative 'engine'
class ExternalDisplay
  attr_accessor :current_floor,:move_direction, :move_to_floor_number
  def initialize()
    @current_floor=1
    @move_direction="fixed"
  end
  def get_move_direction(move_to_floor_number)
      if current_floor<move_to_floor_number
          return "up"
      elseif current_floor==move_to_floor_number
          return "stand on place"
      else
          return "down"
      end
  end
  public
  def show_display_info
     @move_direction=get_move_direction(move_to_floor_number)
     puts "Current floor number is: "+@current_floor.to_s+", move to floor:"+move_to_floor_number.to_s+",move direction: "+@move_direction  
  end
  def show_display_info_on_begin
    puts "Current floor number is: "+current_floor.to_s
  end
end
class InternalDisplay < ExternalDisplay
    attr_accessor :speed,:total_weight,:passengers
    def initialize()
        super()
        @speed=10
        @total_weight = 0
    end
    public
    def show_display_info
      print super()
      puts "Elevator speed is: "+ speed.to_s+", total waight is "+total_weight.to_s + ", current passengers amount: "+passengers.to_s
    end
end

